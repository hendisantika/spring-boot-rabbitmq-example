package com.hendisantika.springbootrabbitmqexample.service;

import com.hendisantika.springbootrabbitmqexample.command.ProductForm;
import com.hendisantika.springbootrabbitmqexample.domain.Product;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rabbitmq-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/03/18
 * Time: 18.46
 * To change this template use File | Settings | File Templates.
 */
public interface ProductService {
    List<Product> listAll();

    Product getById(Long id);

    Product saveOrUpdate(Product product);

    void delete(Long id);

    Product saveOrUpdateProductForm(ProductForm productForm);

    void sendProductMessage(String id);
}
