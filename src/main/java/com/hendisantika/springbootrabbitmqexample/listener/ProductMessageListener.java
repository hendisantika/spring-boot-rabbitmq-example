package com.hendisantika.springbootrabbitmqexample.listener;

import com.hendisantika.springbootrabbitmqexample.domain.Product;
import com.hendisantika.springbootrabbitmqexample.repository.ProductRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rabbitmq-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/03/18
 * Time: 17.43
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ProductMessageListener {

    private static final Logger log = LogManager.getLogger(ProductMessageListener.class);
    private ProductRepository productRepository;

    public ProductMessageListener(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * This method is invoked whenever any new message is put in the queue.
     * See {@link com.hendisantika.springbootrabbitmqexample.SpringBootRabbitmqExampleApplication} for more details
     *
     * @param message
     */
    public void receiveMessage(Map<String, String> message) {
        log.info("Received <" + message + ">");
        Long id = Long.valueOf(message.get("id"));
        Product product = productRepository.findById(id).orElse(null);
        product.setMessageReceived(true);
        product.setMessageCount(product.getMessageCount() + 1);

        productRepository.save(product);
        log.info("Message processed...");
    }
}